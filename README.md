# OpenML dataset: treepipit

https://www.openml.org/d/40505

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data on the population density of tree pipits, Anthus trivialis, in Franconian oak forests including variables describing the forest ecosystem.
This data is taken from R package coin.
This study is based on fieldwork conducted in three lowland oak forests in the Franconian region of northern Bavaria close to Uffenheim, Germany. Diurnal breeding birds were sampled five times, from March to June 2002, using a quantitative grid mapping. Each grid was a one-hectare square. In total, 86 sample sites were established in 9 stands. All individuals were counted in time intervals of 7 min/grid during slow walks along the middle of the grid with a stop in the centre. Environmental factors were measured for each grid.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40505) of an [OpenML dataset](https://www.openml.org/d/40505). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40505/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40505/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40505/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

